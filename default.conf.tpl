server {
    listen ${LISTEN_PORT};

    location /statis {
        alias /vol/static;
    }

    location / {
        uwsgi_pass                ${APP_HOST}:${APP_PORT};
        include                   /etc/nginx/uwsgsi_params;
        client_max_body_size      10M;
    }
}